(function () {
    'use strict';

    angular.module('app').controller('iecController', ['$http', 

        function ($http) {
            var vm = this;
            vm.title = 'IEC';
            vm.error = {msg: ''};
            vm.success = '';

            // Validate the input
            var validate = function () {
                var check = true;
                if (isNaN(vm.iec.code) || vm.iec.code.length < 10) {
                    vm.error = {msg: 'IEC not proper'};
                    check = false;
                }
                if (vm.iec.name.length < 3) {
                    vm.error = {msg: vm.error.msg + ' Name not proper'};
                    check = false;
                }
                return check;
            }

            // Call the service to fetch 
            vm.send = function() {                
                if (validate()) {
                    vm.error = {msg: ''};
                    vm.success = '';
                    $http.post('iec/' + vm.iec.code + '/' + vm.iec.name)
                        .success(function(result, status, headers, config){
                            console.log(status);
                            console.log(result);
                            vm.success = result.success[0];
                        })
                        .error(function(result, status){
                            console.log(result.error);
                            vm.error = {msg: result.error};
                            console.log(status);
                        })
                }
                
            }
        }
    ])

})();